# Análisis de Redes sociales en Salud


El presente trabajo surge de los datos generados para el Estudio de las ENT para  pacientes que se atienden entre 2015 y 2016 en la Facultad de Odontología de la Universidad de la República (Álvarez y Massa, 2020), a través de una muestra probabilística obtenida mediante un diseño sistemático de pacientes.
 Del estudio madre surge el  siguiente trabajo que consiste en un análisis de redes sobre los datos, donde se construye una red formada por los pacientes que se toman como  nodos. Se considera que dos individuos están conectados si comparten al menos un factor de riesgo o patología, que aparecen consignados  como 11 variables binarias . 

 Posteriormente, se consideró un umbral más exigente para definir que dos individuos se encuentran conectados por un eje,mediante la aplicación de una función de filtrado. De manera arbitraria, se consideró la red formada por pacientes que comparten al menos cinco patologías. Más allá de que esto permite descartar asociaciones “débiles” y facilita la visualización de la red, es interesante en la medida que recoge información sobre potenciales relaciones de comorbilidad.
 Finalmente a través de diferentes algoritmos de detección de comunidades  se elaboran los perfiles epidemiológicos.
